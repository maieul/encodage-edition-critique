\documentclass[transnotheorems,noamsthm]{beamer}
\usepackage{fontspec,polyglossia,xunicode,hyperref,csquotes}
\usepackage{graphicx}
\graphicspath{{img/}{}}
\usetheme[]{Boadilla}
\usepackage{metalogo}
\usepackage[noeledsec,noend,series={A,B}]{reledmac}
\newenvironment{slide}{%
  \begin{frame}
  <presentation>\mode<presentation>{\frametitle{\insertsubsection}}%
  }%
  {\end{frame}}
\newenvironment{slide*}{\begin{frame}<presentation>[<1>]\mode<presentation>{\frametitle{\insertsubsection}}}{\end{frame}}

\AtBeginSection{
  \begin{frame}
    \sectionpage
  \end{frame}
}

\beamerdefaultoverlayspecification{<+->}
\setmainfont{Linux Libertine O}
\setmainlanguage{english}
\usepackage{minted}
\newcounter{code}
\resetcounteronoverlays{code}
\usepackage{hyperref}
\newcommand{\package}[1]{\emph{#1}}
\hypersetup{bookmarksdepth=6}
\NewDocumentCommand{\code}{mmo}{%
  \stepcounter{code}%
  \begin{block}{code \thecode: #2}%
			\tiny\IfNoValueF{#3}{#3}%
        \inputminted[linenos=true,breaklines=true]{latex}{code/#1}%
  \end{block}
	}
\setcounter{tocdepth}{1}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}%remove navigation symbols
\newcommand{\shortcode}{\mintinline{latex}}
\usepackage{qrcode}
\newcommand{\linkintitlepage}[1]{%
  \bgroup
    \tiny
    \vfill
    \parbox[c][2cm]{0.8\textwidth}{
      \url{#1}
      \vfill
      Licence Creative Commons France 3.0 - Paternité - Partage à l'identique
    }
    \hfill \qrcode{#1}
  \egroup
}

\author{Maïeul Rouquette}
\title{Producing a critical edition with \LaTeX,}
\subtitle{or the primacy of the typography}
\institute{Université de Lausanne --- IRSB}
\date{2019-02-01}

\begin{document}


\begin{frame}
	\titlepage
  {
    \tiny
    \linkintitlepage{https://geekographie.maieul.net/224}
  }
\end{frame}

\begin{frame}
\tableofcontents
\end{frame}

\section{A brief historical survey}

\subsection{At the beginning was \TeX\ and \LaTeX}

\begin{slide}
  \begin{itemize}
    \item Donald Knuth (1938 - ) \enquote{The Yoda of the Silicon Valley}
    \item \emph{The Art of Computer Programming}
    \item Unsatisfied by the (digital) typography of its publisher, he created \TeX\ (1976)
    \item Today, only some people use again the original \TeX, most people use another \enquote{flavor} of \TeX, like pdf\TeX, \XeTeX\ or Lua\TeX
  \end{itemize}
\end{slide}

\begin{slide}
  \begin{itemize}
      \item \TeX\ provides tools to manage typographical problems such as:
        \begin{itemize}
          \item optimal line breaks;
          \item footnotes insertion dealing with page breaks;
        \end{itemize}
      \item \TeX\ is also a programming language (Turing complete)
      \item It allows to define macro in order to wrap typographical formatting inside structural / logical macros
      \item \LaTeX,  created in 1983, is a set of macros for \TeX\ (a \enquote{format})
      \item Today, \LaTeX\ is the most used format based on \TeX
  \end{itemize}
\end{slide}

\subsection{\TeX\, a high quality typographical composer}
\begin{slide}
  \centering
  \includegraphics[height=0.8\textheight]{comparison.pdf}

  \url{http://www.rtznet.nl/zink/latex.php?lang=nl}
\end{slide}
\subsection{And came \package{edmac} and its successors}

\begin{slide}
  \begin{itemize}
    \item \alert{\package{edmac}} was created to typeset critical edition with \TeX\  (not with \LaTeX!):
John Lavignino and  Dominik Wujastyk (1988-1989)
    \item \alert{\package{ledmac}} is a port of \package{edmac} to be used with \LaTeX: Peter Wilson (2003)
    \item \alert{\package{eledmac}} (2012) and \alert{\package{reledmac}} (2015) are derivated from \package{ledmac} with more features and setups: Maïeul Rouquette
  \end{itemize}
\end{slide}

\section{How do \LaTeX\ and \package{reledmac} work?}

\subsection{\LaTeX's working}

\begin{slide}
  \begin{itemize}
    \item Author writes a text into a text editor, generalist (\package{Atom}, \package{vim}, \ldots) or specialized (\TeX Shop, \TeX Studio, \ldots)
    \item He or she inserts commands / macros into the file to mark the logical structures (sectionning,  footnotes, indexing, bibliography, etc.)
    \item He or she runs \LaTeX\  and gets a PDF output
    \item Sometime, we need more than one run of \LaTeX\  and we need to call auxiliary softwares (indexing, bibliography)
    \item All these runs can be automated with tools like \package{latexmk}
    \item \LaTeX\  has a lot of packages to manage specific problems (like \package{reledmac})
  \end{itemize}
\end{slide}

\subsection{\package{reledmac}'s working}
  \Xarrangement{paragraph}
  \Xnumberonlyfirstinline
  \Xsymlinenum{$||$}
\begin{slide}
  \code{reledmac_mwe.tex}{Example of critical edition typeset with \package{reledmac}}
% Typographical settings
  \null\hfill\begin{minipage}{\textwidth}
    %Duplication de code en attendant la résolution complète de https://github.com/maieul/ledmac/issues/787
% Critical edition itself
    \beginnumbering
    \pstart
    The little \edtext{cat}{\Afootnote{A: dog}} \edtext{died}{\Afootnote{B: passed away}}.
    It failed from the tower.
    Why is it \edtext{always}{\Afootnote{C: \emph{om.}}} a little cat that dies and never a pope that fails  from the  \edtext{tower}{\Afootnote{AD: \emph{add.} to the street}}?
    \pend
    \endnumbering
  \end{minipage}\hfill\null
\end{slide}

\subsection{What does \package{reledmac} provide?}
\begin{slide}
  \begin{itemize}
    \item \alert{Four types of note}: critical footnote, critical endnote, familiar footnote, sidenote
    \item \alert{Multiple levels of note} (except for sidenote): A to E by default
    \item \alert{Multiple settings for note}: formatting, line number managing, etc.
    \item \alert{Tools to manage auxiliary problems} like parallel typesetting (\emph{reledpar}), short forms of lemmas, nested lemmas,  ambiguous lemmas, poetry typesetting, indexing, manuscript apparatus,   etc.
  \end{itemize}
\end{slide}


\section{What does it mean about the concept of critical editions?}

\subsection{Critical edition as an annotated text}
\begin{slide}
  \onslide<1->{A critical edition with \package{reledmac} is considered as:}
  \begin{itemize}
    \item<2-> a \alert{numbered text} (\shortcode|\beginnumbering|\ldots\shortcode|\endnumbering|)
    \item<3-> containing \alert{paragraphs} (\shortcode|\pstart|\ldots\shortcode|\pend|)
    \item<4-> with \alert{lemmas} (\shortcode|\edtext|)
    \item<5-> which are associated to \alert{notes} (\shortcode|\Afootnote|)
  \end{itemize}
\end{slide}

\subsection{Semantic annotation with  \package{reledmac}?}
\begin{slide}
  \code{reledmac_semantic_annotation.tex}{Example of semantic annotation with \package{reledmac}}
\end{slide}

\subsection{Critical edition as an achieved text}

\begin{slide}
  \begin{itemize}
    \item Edited text is primary, variants are secondary
    \item In other terms: with \package{reledmac}, there is a supremacy of the editor other the edited text and it witnesses
    \item \package{reledmac} is designed to \alert{typeset} critical edition, not to \alert{structure} or to \alert{conceive} it
  \end{itemize}
\end{slide}



\subsection{\enquote{Variants first} encoding with \package{reledmac}?}

\begin{slide}
  \begin{itemize}
    \item Is it possible to conceive a critical edition with \package{reledmac} that would consider text as a series of variants, some retained, other ones rejected?
    \item That was the aims of (abonned) \package{eledform} project
    \item Why it was not so much a good idea?
      \begin{itemize}
        \item Complex to implement fully with \TeX\ language (maybe easier with \LuaTeX)
        \item Only one possible output: PDF, and so printed book, not website (not easily)
        \item Performance issues: logical structure much be translated into typographical structure at each run
        \item Redundant with other existing tools (TEI, \package{Stemmaweb})
      \end{itemize}
  \end{itemize}
\end{slide}





\section{Conclusion: \LaTeX\ as a final tool}
\begin{slide}
  \begin{itemize}
    \item Keeping \LaTeX\  as a final tool to typeset critical edition:
      \begin{itemize}
        \item A lot of typographical tools for critical editions in \package{reledmac}
        \item Interaction with other packages of \LaTeX: to draw \emph{stemma codicum}, bibliography, etc.
        \item High typography quality
        \item Scriptable and versionnable
        \item Free / Open source software
      \end{itemize}
    \item Using other tools to conceive and structure editions, then export to \LaTeX
  \end{itemize}
\end{slide}

\begin{slide}
  \vfill
  \hfill
  \includegraphics[height=0.8\textheight]{famille.jpg}
  \hfill\null
  \vfill
  \hfill\tiny Credit: Duane Dubby
\end{slide}
\end{document}
